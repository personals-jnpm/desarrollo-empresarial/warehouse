FROM maven:3.6.1-jdk-8-alpine AS build
RUN mkdir -p /workspace
WORKDIR /workspace

COPY pom.xml .
COPY warehouse-ejb /workspace/warehouse-ejb
COPY warehouse-web /workspace/warehouse-web
COPY warehouse-ear /workspace/warehouse-ear
RUN mvn -f pom.xml clean package

FROM jboss/wildfly:latest
RUN /opt/jboss/wildfly/bin/add-user.sh admin-wf 123456 --silent
# ADD customization/standalone.xml /opt/jboss/wildfly/standalone/configuration/
COPY --from=build /workspace/warehouse-ear/target/warehouse-ear-1.0-SNAPSHOT.ear /opt/jboss/wildfly/standalone/deployments/
EXPOSE 80 9990 8009 $PORT
CMD /opt/jboss/wildfly/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0 -c standalone.xml -Djboss.http.port=$PORT