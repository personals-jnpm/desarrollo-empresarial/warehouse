package co.edu.usta.warehouse.utilities;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public class ShowMessage {

    private static void build(Severity type, String title, String message, String identifier) {
        if (identifier.equals("")) {
            identifier = null;
        }

        FacesMessage mensaje = new FacesMessage(type, title, message);
        FacesContext.getCurrentInstance().addMessage(identifier, mensaje);
    }

    private static void error(String title, String message, String identifier) {
        ShowMessage.build(FacesMessage.SEVERITY_ERROR, title, message, identifier);
    }

    private static void success(String title, String message, String identifier) {
        ShowMessage.build(FacesMessage.SEVERITY_INFO, title, message, identifier);
    }

    private static void warning(String title, String message, String identifier) {
        ShowMessage.build(FacesMessage.SEVERITY_WARN, title, message, identifier);
    }

    private static void fatal(String title, String message, String identifier) {
        ShowMessage.build(FacesMessage.SEVERITY_FATAL, title, message, identifier);
    }

    public static void screen(String propertiesFile, String type, String title, String message,
                                String identifier) {
        String titleFinal= ResourceBundle.getBundle(propertiesFile).getString(title);
        String messageFinal= ResourceBundle.getBundle(propertiesFile).getString(message);

        switch (type) {
            case "success":
                ShowMessage.success(titleFinal, messageFinal, identifier);
                break;
            case "error":
                ShowMessage.error(titleFinal, messageFinal, identifier);
                break;
            case "warning":
                ShowMessage.warning(titleFinal, messageFinal, identifier);
                break;
            default:
                ShowMessage.fatal(titleFinal, messageFinal, identifier);
                break;
        }
    }

}

