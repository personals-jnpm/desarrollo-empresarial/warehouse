package co.edu.usta.warehouse.controllers;

import co.edu.usta.warehouse.models.Book;
import co.edu.usta.warehouse.services.LibraryService;
import co.edu.usta.warehouse.utilities.Properties;
import lombok.Data;
import org.primefaces.PrimeFaces;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named("beanBook")
@SessionScoped
@Data
public class BookController implements Serializable {

    @EJB
    LibraryService<Book> bookService;

    private List<Book> books;

    private Book selectedBook;

    private List<Book> selectedBooks;

    @PostConstruct
    public void init() {
        books = bookService.findAll(Book.class);
    }

    public void initObject() {
        this.selectedBook = new Book();
    }

    public void saveBook() {
        if (this.selectedBook.getId() == null) {
            this.books.add(this.selectedBook);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    getMessage("bookSuccessMessage"),
                    getMessage("bookAddedMessage")));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                    getMessage("bookSuccessMessage"),
                    getMessage("bookUpdatedMessage")));
        }

        PrimeFaces.current().executeScript("PF('manageBookDialog').hide()");
        PrimeFaces.current().ajax().update("form:messages", "form:dt-books");
    }

    public void deleteBook() {
        this.books.remove(this.selectedBook);
        this.selectedBook = null;
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
                getMessage("bookSuccessMessage"),
                getMessage("bookDeletedMessage")));
        PrimeFaces.current().ajax().update("form:messages", "form:dt-books");
    }

    public String getDeleteButtonMessage() {
        if (hasSelectedBooks()) {
            int size = this.selectedBooks.size();
            return size > 1 ? size + " " + getMessage("bookDeleteMultipleItemsLabel") :
                    getMessage("bookDeleteOneItemLabel");
        }

        return getMessage("bookDeleteLabel");
    }

    public boolean hasSelectedBooks() {
        return this.selectedBooks != null && !this.selectedBooks.isEmpty();
    }

    public void deleteSelectedBooks() {
        this.books.removeAll(this.selectedBooks);
        this.selectedBooks = null;
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(getMessage("bookSuccessMessage"),
                getMessage("booksDeletedMessage")));
        PrimeFaces.current().ajax().update("form:messages", "form:dt-books");
        PrimeFaces.current().executeScript("PF('dtProducts').clearFilters()");
    }

    private String getMessage(String key) {
        return Properties.getBundle("labels.messages", key);
    }

}
