insert into table_example (createAt, name) values ('2021-01-01', 'Jhon');
insert into table_example (createAt, name) values ('2021-05-02', 'Nick');
insert into table_example (createAt, name) values ('2021-03-03', 'Ana');
insert into table_example (createAt, name) values ('2021-06-04', 'María');

INSERT INTO public.book_types (id, type_name, active, created_date, update_date) VALUES (1, 'narrativo', true, '2022-01-19', '2022-01-19');
INSERT INTO public.authors (id, names, lastnames, nationality, birthday, active, created_date, update_date) VALUES (1, 'Andres', 'Orozco', 'Colombiana', '2000-01-19', true, '2022-01-19', '2022-01-19');
INSERT INTO public.books (id, isbn, title, original_publication, active, image, created_date, update_date, id_book_types, id_authors) VALUES (1, '131564321', 'La oculta', '2022-01-18', true, 'avatar-f-3.png', '2022-01-19', '2022-01-20', 1, 1);
INSERT INTO public.books (id, isbn, title, original_publication, active, image, created_date, update_date, id_book_types, id_authors) VALUES (2, '132132132', 'Otro libro', '2022-01-18', true, 'avatar-f-3.png', '2022-01-19', '2022-01-20', 1, 1);
INSERT INTO public.books (id, isbn, title, original_publication, active, image, created_date, update_date, id_book_types, id_authors) VALUES (3, '165456646', 'Raíces', '2022-01-18', true, 'avatar-f-3.png', '2022-01-19', '2022-01-20', 1, 1);
