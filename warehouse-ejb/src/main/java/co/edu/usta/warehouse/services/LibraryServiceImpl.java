package co.edu.usta.warehouse.services;

import co.edu.usta.warehouse.repositories.RepositoryJPA;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.List;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class LibraryServiceImpl<T> implements LibraryService<T> {

    @EJB
    RepositoryJPA<T> repositoryJPA;

    @Override
    public void save(T entity) {
        repositoryJPA.save(entity);
    }

    @Override
    public boolean update(T entity) {
        return repositoryJPA.update(entity);
    }

    @Override
    public List<T> findAll(Class<T> entity) {
        return repositoryJPA.findAll(entity);
    }

    @Override
    public List<T> findAll(Class<T> entity, String column) {
        return repositoryJPA.findAll(entity, column);
    }

    @Override
    public List<T> listNQUERY(Class<T> entity, String namedQuery) {
        return repositoryJPA.listNQUERY(entity, namedQuery);
    }

    @Override
    public List<T> listNQUERY(Class<T> entity, String namedQuery, String column, Object value) {
        return repositoryJPA.listNQUERY(entity, namedQuery, column, value);
    }

    @Override
    public T find(Class<T> entity, Integer id) {
        return repositoryJPA.find(entity, id);
    }

    @Override
    public List<T> findByField(Class<T> entity, String column, String value) {
        return repositoryJPA.findByField(entity, column, value);
    }
}
