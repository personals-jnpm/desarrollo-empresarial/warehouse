package co.edu.usta.warehouse.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name="users")
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="accumulated_fine")
	private double accumulatedFine;

	private String dni;

	private String email;

	private String lastnames;

	private String names;

	private String password;

	private String username;

	private boolean active;

	private String image;

	@OneToMany(mappedBy="user")
	@ToString.Exclude
	private List<Loan> loans;

	@ManyToOne
	@JoinColumn(name="id_roles")
	private Role role;

	@Column(name="created_date")
	private LocalDateTime createdDate;

	@Column(name="update_date")
	private LocalDateTime updateDate;

	@PrePersist
	public void prePersist(){
		createdDate = LocalDateTime.now();
		updateDate = createdDate;
	}

	public Loan addLoan(Loan loan) {
		getLoans().add(loan);
		loan.setUser(this);

		return loan;
	}

	public Loan removeLoan(Loan loan) {
		getLoans().remove(loan);
		loan.setUser(null);

		return loan;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		User user = (User) o;
		return id != null && Objects.equals(id, user.id);
	}

	@Override
	public int hashCode() {
		return getClass().hashCode();
	}
}