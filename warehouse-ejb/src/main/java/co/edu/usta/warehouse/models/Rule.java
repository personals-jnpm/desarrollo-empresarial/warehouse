package co.edu.usta.warehouse.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name="rules")
public class Rule implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="amount_fine")
	private double amountFine;

	@Column(name="loan_days")
	private Integer loanDays;

	@Column(name="maximum_loans")
	private Integer maximumLoans;

	private boolean active;

	@OneToMany(mappedBy="rule")
	@ToString.Exclude
	private List<Role> roles;

	@Column(name="created_date")
	private LocalDateTime createdDate;

	@Column(name="update_date")
	private LocalDateTime updateDate;

	@PrePersist
	public void prePersist(){
		createdDate = LocalDateTime.now();
		updateDate = createdDate;
	}

	public Role addRole(Role role) {
		getRoles().add(role);
		role.setRule(this);

		return role;
	}

	public Role removeRole(Role role) {
		getRoles().remove(role);
		role.setRule(null);

		return role;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Rule rule = (Rule) o;
		return id != null && Objects.equals(id, rule.id);
	}

	@Override
	public int hashCode() {
		return getClass().hashCode();
	}
}