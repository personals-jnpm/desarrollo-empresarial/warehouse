package co.edu.usta.warehouse.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name="authors")
public class Author implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private LocalDateTime birthday;

	private String lastnames;

	private String names;

	private String nationality;

	private boolean active;

	private String image;

	@OneToMany(mappedBy="author")
	@ToString.Exclude
	private List<Book> books;

	@Column(name="created_date")
	private LocalDateTime createdDate;

	@Column(name="update_date")
	private LocalDateTime updateDate;

	public Author(Integer id, LocalDateTime birthday, String lastnames, String names, String nationality, boolean active, String image) {
		this.id = id;
		this.birthday = birthday;
		this.lastnames = lastnames;
		this.names = names;
		this.nationality = nationality;
		this.active = active;
		this.image = image;
	}

	@PrePersist
	public void prePersist(){
		createdDate = LocalDateTime.now();
		updateDate = createdDate;
	}

	public Book addBook(Book book) {
		getBooks().add(book);
		book.setAuthor(this);

		return book;
	}

	public Book removeBook(Book book) {
		getBooks().remove(book);
		book.setAuthor(null);

		return book;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Author author = (Author) o;
		return id != null && Objects.equals(id, author.id);
	}

	@Override
	public int hashCode() {
		return getClass().hashCode();
	}
}