package co.edu.usta.warehouse.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name="copies")
public class Copy implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String barcode;

	private String editorial;

	private LocalDateTime publication;

	private Integer state;

	private boolean active;

	@ManyToOne
	@JoinColumn(name="id_books")
	private Book book;

	@OneToMany(mappedBy="copy")
	@ToString.Exclude
	private List<Loan> loans;

	@Column(name="created_date")
	private LocalDateTime createdDate;

	@Column(name="update_date")
	private LocalDateTime updateDate;

	@PrePersist
	public void prePersist(){
		createdDate = LocalDateTime.now();
		updateDate = createdDate;
	}

	public Loan addLoan(Loan loan) {
		getLoans().add(loan);
		loan.setCopy(this);

		return loan;
	}

	public Loan removeLoan(Loan loan) {
		getLoans().remove(loan);
		loan.setCopy(null);

		return loan;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Copy copy = (Copy) o;
		return id != null && Objects.equals(id, copy.id);
	}

	@Override
	public int hashCode() {
		return getClass().hashCode();
	}
}