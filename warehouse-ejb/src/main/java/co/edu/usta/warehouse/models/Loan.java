package co.edu.usta.warehouse.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name="loans")
public class Loan implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="maximum_return_date")
	private LocalDateTime maximumReturnDate;

	private Integer state;

	private boolean active;

	@ManyToOne
	@JoinColumn(name="id_copies")
	private Copy copy;

	@ManyToOne
	@JoinColumn(name="id_users")
	private User user;

	@Column(name="created_date")
	private LocalDateTime createdDate;

	@Column(name="update_date")
	private LocalDateTime updateDate;

	@PrePersist
	public void prePersist(){
		createdDate = LocalDateTime.now();
		updateDate = createdDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Loan loan = (Loan) o;
		return id != null && Objects.equals(id, loan.id);
	}

	@Override
	public int hashCode() {
		return getClass().hashCode();
	}
}