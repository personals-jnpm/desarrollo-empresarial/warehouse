package co.edu.usta.warehouse.services;

import java.util.List;

public interface LibraryService<T> {

    void save(T entity);

    boolean update(T entity);

    List<T> findAll(Class<T> entity);

    List<T> findAll(Class<T> entity, String column);

    List<T> listNQUERY(Class<T> entity, String namedQuery);

    List<T> listNQUERY(Class<T> entity, String namedQuery, String column, Object value);

    T find(Class<T> entity, Integer id);

    List<T> findByField(Class<T> entity, String column, String value);
}
