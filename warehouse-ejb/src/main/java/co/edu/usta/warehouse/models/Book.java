package co.edu.usta.warehouse.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name="books")
public class Book implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String isbn;

	@Column(name="original_publication")
	private LocalDateTime originalPublication;

	private String title;

	private boolean active;

	private String image;

	@ManyToOne
	@JoinColumn(name="id_authors")
	private Author author;

	@ManyToOne
	@JoinColumn(name="id_book_types")
	private BookType bookType;

	@OneToMany(mappedBy="book")
	@ToString.Exclude
	private List<Copy> copies;

	@Column(name="created_date")
	private LocalDateTime createdDate;

	@Column(name="update_date")
	private LocalDateTime updateDate;

	public Book(Integer id, String isbn, LocalDateTime originalPublication, String title, boolean active, String image, Author author, BookType bookType) {
		this.id = id;
		this.isbn = isbn;
		this.originalPublication = originalPublication;
		this.title = title;
		this.active = active;
		this.image = image;
		this.author = author;
		this.bookType = bookType;
	}

	@PrePersist
	public void prePersist(){
		createdDate = LocalDateTime.now();
		updateDate = createdDate;
	}

	public Copy addCopy(Copy copy) {
		getCopies().add(copy);
		copy.setBook(this);

		return copy;
	}

	public Copy removeCopy(Copy copy) {
		getCopies().remove(copy);
		copy.setBook(null);

		return copy;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Book book = (Book) o;
		return id != null && Objects.equals(id, book.id);
	}

	@Override
	public int hashCode() {
		return getClass().hashCode();
	}
}